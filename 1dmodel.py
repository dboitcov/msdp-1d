#!/usr/bin/env python3
from nutils import *

class Plotter:
    def __init__(self, domain, ns, stride):
        self.domain = domain
        self.ns = ns
        self.stride = stride
    def __call__(self, lhs, i):
        if i % self.stride: return
        x, y, z = self.domain.elem_eval(
          [self.ns.x, 'v' @ self.ns(lhs=lhs), 'μm' @ self.ns(lhs=lhs)],
          ischeme='bezier9', separate=False)
        with plot.PyPlot('solution') as plt:
            plt.subplot(211)
            plt.plot(x, y)
            plt.ylabel('v')
            plt.title(f't={i*self.ns.dt.value*1e3:.0f}ms')
            plt.subplot(212)
            plt.plot(x, z)
            plt.ylabel('μm')
            plt.xlabel('x')

def nonnewtonian(
      nelems: 'number of elements' = 20,
      degree: 'polynomial degree' = 3,
      timestep: 'time step' = 0.001,
      diameter: 'diameter of needle' = 300e-6,
      length: 'length' = 3.87e-3,
      density: 'solder density' = 11.07e3,
      pressure: 'pressure drop' = 50e3,
      viscosity: 'dynamic visc' = 42,
      tdisp: 'pulse duration' = 0.16,
      n: 'exponent of power law' = 0.535,
      K: 'factor of power law' = 131.367,
      plots: 'plot or not' = True,
    ):
    # construct mesh
    verts = numpy.linspace(0, 0.5 * diameter, nelems+1)
    domain, geom = mesh.rectilinear([verts])
    basis = domain.basis('spline', degree=degree)

    # create namespace
    ns = function.Namespace()
    ns.x = geom
    ns.w = basis
    ns.v = 'w_n ?lhs_n'
    ns.f = pressure / length
    ns.K = K
    ns.N = n
    ns.γm = 'sqrt(1 + (v_,0)^2)' # absolute value, modified near 0
    ns.μm = 'K γm^(N - 1)'
    ns.ρ = density
    ns.dt = timestep
    ns.R = diameter / 2

    # construct essential constraints
    kwargs = dict(geometry=ns.x, degree=degree*2)
    sqr = domain.boundary['left'].integral('v^2' @ ns, **kwargs)
    cons = solver.optimize('lhs', sqr, droptol=1e-15)
    plotter = Plotter(domain, ns, 20)
    lhs = numpy.zeros(cons.shape)

    # find lhs such that res == 0 and substitute this lhs in the namespace
    res = domain.integral('μm v_,i w_n,i - f w_n' @ ns, **kwargs)
    inertia = domain.integral('ρ v w_n / dt' @ ns, **kwargs)
    integrator = solver.cranknicolson('lhs', residual=res, inertia=inertia,
      lhs0=lhs, timestep=timestep, constrain=cons, newtontol=1e-10)
    fluxes = []
    for i, lhs in log.enumerate('t', integrator):
        plotter(lhs, i)
        flux = 2.*numpy.pi*domain.integrate('v (R - x_0)' @ ns(lhs=lhs), geometry=ns.x, degree=degree*2)
        fluxes.append(flux)
        if i*timestep > tdisp: break
            
    volume = sum(fluxes) * timestep * 1e18 * 1e-6
    if not plots: return volume # in 10^6 μm^3
    log.user('vol:', volume, 'mln um^3')

def newtonian(
      nelems: 'number of elements' = 10,
      degree: 'polynomial degree' = 1,
      radius: 'radius of needle' = 225e-6,
      length: 'length' = 3.75e-3,
      pressure: 'pressure drop' = 66e3,
      viscosity: 'dynamic visc' = 42,
      tdisp: 'pulse duration' = 0.16,
      plots: 'plot or not' = True,
    ):
    # construct mesh
    verts = numpy.linspace(0, radius, nelems+1)
    domain, geom = mesh.rectilinear([verts])
  
    # create namespace
    ns = function.Namespace()
    ns.x = geom
    ns.basis = domain.basis('spline', degree=degree)
    ns.v = 'basis_n ?lhs_n'
    ns.f = pressure/length
    ns.viscosity = viscosity
  
    # construct residual
    res = domain.integral('basis_n,i viscosity v_,i + f basis_n' @ ns, geometry=ns.x, degree=degree*2)
  
    # construct essential constraints
    sqr = domain.boundary['left'].integral('v^2' @ ns, geometry=ns.x, degree=degree*2)
    cons = solver.optimize('lhs', sqr, droptol=1e-15)
  
    # find lhs such that res == 0 and substitute this lhs in the namespace
    lhs = solver.solve_linear('lhs', res, constrain=cons)
    ns = ns(lhs=lhs)
  
    # find droplet volume
    ns.R = radius
    flux = 2.*numpy.pi*domain.integrate('-v (R - x_0)' @ ns, geometry=ns.x, degree=degree*2)
    volume = tdisp * flux * 1e18 * 1e-6
    if not plots: return volume # in 10^6 μm^3
    log.user('vol:', volume, 'mln um^3')
  
    # plot solution
    x, y = domain.elem_eval([ns.x, ns.v], ischeme='bezier9', separate=False)
    with plot.PyPlot('solution') as plt:
        plt.plot(x, y)
        plt.xlabel('x')
        plt.ylabel('v')

def parameterstudy():
    N, vol = [], []
    for n in log.range('nelems', 1, 8):
        nelems = 2**n
        N.append(nelems)
        droplet_volume = newtonian(nelems=nelems, plots=False)
        vol.append(droplet_volume)
    with plot.PyPlot('fig') as fig:
        fig.title('Large droplet volume grid conv')
        fig.semilogx(N, vol)
        fig.xlabel('number of elements')
        fig.ylabel('volume in [$10^6 \cdot\; μm^3$]')

def powerlaw(K=131, N=0.54):
  γ = numpy.linspace(-10, 10, 33)
  μ = K * numpy.abs(γ)**(N - 1)
  γm = numpy.sqrt(1 + γ**2)
  μm = K * γm**(N - 1)
  log.user('γm:', γm)
  log.user('μm:', μm)
  with plot.PyPlot('fig') as fig:
      fig.plot(γ, μ, label='original')
      fig.plot(γ, μm, label='modified')
      fig.legend()

if __name__ == '__main__':
	cli.choose(parameterstudy, powerlaw, nonnewtonian, newtonian)
