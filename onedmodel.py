#!/usr/bin/env python3
from nutils import *
from scipy import interpolate
import os
pycols = ("#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd",
          "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf")
lmap = lambda *a: list(map(*a))

def add_area(fig, height):
    v = numpy.asarray(fig.gca().get_yticks())
    a = 3*v/height*1e2 # 10^6 μm2 --> 10^4 μm2
    fig2 = fig.twinx()
    fig2.set_ylabel(f'Droplet area [$10^4\;μm^2$]')
    fig2.set_yticks(v)
    fig2.set_yticklabels(lmap(lambda ai: '%.0f'%ai, a))

class Plotter:
    def __init__(self, domain, ns, stride, comsol=None):
        self.domain = domain
        self.ns = ns
        self.stride = stride
        if comsol:
            fname = f'velocity_profile_{comsol}_COMSOL_nonnewtonian.txt'
            r, v = numpy.loadtxt(fname, comments='%').T
            self.comsol = interpolate.interp1d(r, v*1e-3, kind='cubic')
    def __call__(self, lhs, i):
        if i % self.stride: return
        x, y, z = self.domain.elem_eval(
          [self.ns.x*1e3, self.ns(lhs=lhs).v*1e3, 'μm' @ self.ns(lhs=lhs)],
          ischeme='bezier9', separate=False)
        with plot.PyPlot('solution') as plt:
            plt.subplot(211)
            plt.plot(x, y, label='1D')
            if hasattr(self, 'comsol'):
                plt.plot(x, self.comsol(x), label='3D @160ms')
                plt.legend()
            plt.ylabel('$v$ [$mm/s$]')
            plt.title(f'$t={i*self.ns.dt.value*1e3:.0f}ms$')
            plt.subplot(212)
            plt.semilogy(x, z)
            plt.ylabel('$μ_m$ [$Pa\;s$]')
            plt.xlabel('$r$ [$mm$]')

class NonNewtonian(function.Namespace):
    def __init__(self, geom, basis, force, density, timestep, K, n, maxvisc):
        super().__init__()
        self.x = geom
        self.w = basis
        self.v = 'w_n ?lhs_n'
        self.f = force
        self.K = K
        self.N = n
        self.asq = (maxvisc/K)**(2/(n-1))
        self.γm = f'sqrt(asq + (v_,0)^2)' # absolute value, modified near 0, see powerlaw()
        self.μm = 'K γm^(N - 1)'
        self.ρ = density
        self.dt = timestep

def nonnewtonian(
      nelems: 'number of elements' = 10,
      degree: 'polynomial degree' = 3,
      timestep: 'time step' = 0.001,
      diameter: 'diameter of needle' = 300e-6,
      length: 'length' = 3.75e-3,
      density: 'solder density' = 11.1e3,
      pressure: 'pressure drop' = 51e3,
      viscosity: 'dynamic visc' = 42,
      tdisp: 'pulse duration' = 0.16,
      n: 'exponent of power law' = 0.54,
      K: 'factor of power law' = 131.,
      maxvisc: 'shear rate regularization' = 1e3,
      plots: 'plot or not' = True,
    ):
    # construct mesh
    verts = numpy.linspace(0, 0.5 * diameter, nelems+1)
    domain, geom = mesh.rectilinear([verts])
    basis, time = function.chain([
      domain.basis('spline', degree=degree), [1.]])

    # create namespace
    ns = NonNewtonian(geom, basis, pressure / length, density, timestep,
                      K, n, maxvisc)
    ns.τ = time
    ns.t = 'τ_k ?lhs_k'
    A = 2.44e-3
    ns.f = (pressure / length) * function.piecewise(ns.t, (tdisp,),
      1 - A/(ns.t + A), A/(ns.t - tdisp + A))

    # construct essential constraints
    kwargs = dict(geometry=ns.x, degree=degree*2)
    sqr = domain.boundary['left'].integral('v^2' @ ns, **kwargs)
    cons = solver.optimize('lhs', sqr, droptol=1e-15)
    comsol = {0.0003:'small', 0.00045:'large'}.get(diameter, None)
    log.user(comsol)
    plotter = Plotter(domain, ns, 20, comsol=comsol)
    lhs = lhsk = numpy.zeros(cons.shape)

    # find lhs such that res == 0 and substitute this lhs in the namespace
    ns.vk = 'w_n ?lhsk_n'
    ns.R = 0.5 * diameter
    times, pressures, fluxes = [], [], []
    for i in log.range('time', int(1.25*tdisp/timestep)+1):
        res = domain.integral('ρ (v - vk) w_n / dt + 2 μm v_,i w_n,i - f w_n'
          @ ns(lhsk=lhsk), **kwargs)
        cons[-1] = i*timestep
        lhs = solver.newton('lhs', residual=res, lhs0=lhs, constrain=cons).solve(tol=1e-8)
        lhsk = lhs

        flux = 2.*numpy.pi*domain.integrate('-v (R - x_0)' @ ns(lhs=lhs), **kwargs)
        fluxes.append(flux)
        if plots:
            times.append(ns(lhs=lhs).t.eval()[0])
            pressures.append(ns(lhs=lhs).f.eval()[0])
            plotter(lhs, i)

    volume = -timestep*sum(fluxes)*1e12 # in mln μm^3 / nL
    log.user(f'diameter: {diameter*1e6:.0f} μm volume: {volume:.2f} nL')
    if not plots: return volume

    times = 1e3 * numpy.asarray(times) # s --> ms
    fluxes = -1e18 * numpy.asarray(fluxes) # m^3/s --> μm^3
    pressures = 1e-3 * numpy.asarray(pressures) * length # Pa/m --> kPa
    data = numpy.asarray([times, fluxes]).T
    numpy.savetxt(f'nonnewt_1d_{comsol}.csv', data)

    with plot.PyPlot('pressure') as plt:
        plt.plot(times, pressures, 'k-')
        plt.title('needle inlet pressure profile')
        plt.xlabel('$t$ [$ms$]')
        plt.ylabel('$p$ [$kPa$]')

    return volume

def newtonian(
      nelems: 'number of elements' = 10,
      degree: 'polynomial degree' = 1,
      radius: 'radius of needle' = 225e-6,
      length: 'length' = 3.75e-3,
      pressure: 'pressure drop' = 66e3,
      viscosity: 'dynamic visc' = 42,
      tdisp: 'pulse duration' = 0.16,
      plots: 'plot or not' = True,
    ):
    # construct mesh
    verts = numpy.linspace(0, radius, nelems+1)
    domain, geom = mesh.rectilinear([verts])
  
    # create namespace
    ns = function.Namespace()
    ns.x = geom
    ns.basis = domain.basis('spline', degree=degree)
    ns.v = 'basis_n ?lhs_n'
    ns.f = pressure/length
    ns.viscosity = viscosity
  
    # construct residual
    res = domain.integral('basis_n,i viscosity v_,i + f basis_n' @ ns, geometry=ns.x, degree=degree*2)
  
    # construct essential constraints
    sqr = domain.boundary['left'].integral('v^2' @ ns, geometry=ns.x, degree=degree*2)
    cons = solver.optimize('lhs', sqr, droptol=1e-15)
  
    # find lhs such that res == 0 and substitute this lhs in the namespace
    lhs = solver.solve_linear('lhs', res, constrain=cons)
    ns = ns(lhs=lhs)
  
    # find droplet volume
    ns.R = radius
    flux = 2.*numpy.pi*domain.integrate('-v (R - x_0)' @ ns, geometry=ns.x, degree=degree*2)
    volume = tdisp * flux * 1e18 * 1e-6
    if not plots: return volume # in 10^6 μm^3
    log.user('vol:', volume, 'mln um^3')
  
    # plot solution
    x, y = domain.elem_eval([ns.x, ns.v], ischeme='bezier9', separate=False)
    with plot.PyPlot('solution') as plt:
        plt.plot(x, y)
        plt.xlabel('x')
        plt.ylabel('v')

def viscsweep(Ks, n, p, d):
    μs, vols = [], []
    for K in log.iter('K', Ks):
        droplet_volume = nonnewtonian(K=K, n=n, pressure=0.625*p, diameter=d,
          nelems=6, timestep=1e-2, plots=False)
        μs.append(K*10**(n-1))
        vols.append(droplet_volume) # μm^3
    Kfunc = interpolate.interp1d(vols, Ks, kind='cubic')
    μfunc = interpolate.interp1d(vols, μs, kind='cubic')
    return Kfunc, μfunc, vols
def thawparsweep(size='large'):
    # parameters
    n = 0.54
    ps = [60e3, 75e3, 90e3, 105e3]
    mp = 1 # second p in ps is of interest
    if size == 'large':
        d = 0.00045
        h = 230 # solder droplet height in μm
        Ks = numpy.linspace(125, 175, 5)
        thaw = {'30h':80.7} # {'3h':74.8, '30h':80.7, '50h':54.4}
    else:
        d = 0.0003
        h = 210
        Ks = numpy.linspace(100, 175, 5)
        thaw = {'30h':15.2} # {'3h':12.1, '30h':15.2, '50h':8.6}

    # simulate
    M, N = len(ps), len(Ks)
    fname = f'{size}.txt'
    data = numpy.loadtxt(fname) if os.path.exists(fname) else numpy.empty((M*N, 4))
    Kfuncs, μfuncs = [], []
    for m, p in log.enumerate('p', ps):
        if os.path.exists(fname):
            Ks, μs, vols = data[m*N:(m+1)*N,1:].T
            Kfunc = interpolate.interp1d(vols, Ks, kind='cubic')
            μfunc = interpolate.interp1d(vols, μs, kind='cubic')
        else:
            Kfunc, μfunc, vols = viscsweep(Ks, n, p, d)
            μs = numpy.array(lmap(lambda v: float(μfunc(v)), vols))
            vols = numpy.array(vols)
        Kfuncs.append(Kfunc)
        μfuncs.append(μfunc)
        data[m*N:(m+1)*N,:] = numpy.concatenate([
          numpy.ones(N)*p, Ks, μs, vols]).reshape(4, N).T
    if not os.path.exists(fname): numpy.savetxt(fname, data)

    # plot vs viscosity
    dvol = max(data[:,3])*1e-2 # text hover distance
    with plot.PyPlot('visc', figsize=(8,5)) as fig:
        for m, (p, Kfunc, μfunc) in log.enumerate('p', zip(ps, Kfuncs, μfuncs)):
            if m == mp:
                for lab, vol in thaw.items():
                    K = float(Kfunc(vol))
                    μ = float(μfunc(vol))
                    log.user(f'{size}|{lab}: μ={μ:.1f} K={K:.2f}')
                    fig.plot(μ, vol, 'x', label=f'${lab}$')
                    fig.text(μ, vol+dvol, f'{μ:.1f} $Pa\;s$, {vol} $10^6\;μm^3$',
                      ha='left', va='bottom')
            minvol, maxvol = data[[m*N, (m+1)*N-1],3]
            vols = numpy.linspace(minvol, maxvol, 65)
            fig.plot(μfunc(vols), vols, '-', label=f'{p/1e3:.0f} $kPa$')
        fig.legend()
        fig.xlabel('Viscosity @10 $s^{-1}$ [$Pa\;s$]')
        fig.ylabel(f'Droplet volume [$10^6\;μm^3$]')
        fig.title(f'Droplet volume of {size} needle')
        add_area(fig, h)

    # plot vs pressure
    with plot.PyPlot('pres', figsize=(8,5)) as fig:
        for n in log.range(n, N):
            μ = data[n,2]
            vols = data[n::N,3]
            pfunc = interpolate.interp1d(vols, ps, kind='cubic')
            minvol, maxvol = vols[[0,-1]]
            vols = numpy.linspace(minvol, maxvol, 65)
            fig.plot(pfunc(vols)/1e3, vols, '-', label=f'μ={μ:.1f} $Pa\;s$ @10 $s^{{-1}}$')
        fig.legend()
        for lab, vol in thaw.items():
            p = ps[mp]*1e-3
            fig.plot(p, vol, 'x')
            fig.text(p, vol+dvol, f'{lab}: {vol} $10^6\;μm^3$',
              ha='left', va='bottom')
        fig.xlabel('Inlet pressure [$kPa$]')
        fig.ylabel(f'Droplet volume [$10^6\;μm^3$]')
        fig.title(f'Droplet volume of {size} needle')
        add_area(fig, h)

def presparsweep(size='large'):
    n = 0.54
    ps = numpy.linspace(60, 110, 5)
    if size == 'large':
        d = 0.00045
        Ks = {'3h':143.20, '30h':137.45, '50h':170.09}
        VOL = 80
    else:
        d = 0.0003
        Ks = {'3h':132.36, '30h':117.06, '50h':159.20}
        VOL = 15
    for lab, K in Ks.items():
        vols = []
        for p in log.iter('p', ps):
            pbar = 0.625*p*1e3
            droplet_volume = nonnewtonian(K=K, n=n, pressure=pbar, diameter=d,
              nelems=6, timestep=1e-2, plots=False)
            vols.append(droplet_volume)
        pfunc = interpolate.interp1d(vols, ps, kind='cubic')
        with plot.PyPlot('fig') as fig:
            fig.plot(pfunc(vols), vols, '.-', label='sweep/fit')
            p = float(pfunc(VOL))
            log.user(f'{size}|{lab}: p={p:.2f}')
            fig.plot(p, VOL, 'x', label=f'${lab}$: {p:.1f} $kPa$')
            fig.legend()
            fig.xlabel('Inlet pressure [$kPa$]')
            fig.ylabel(f'Droplet volume [$nL$]')
            fig.title(f'Droplet volume of {size} needle, {lab} thawed paste')

def diamsweep(K, n, p, ds):
    vols = []
    for d in log.iter('d', ds):
        droplet_volume = 0 if d==0 else nonnewtonian(
          K=K, n=n, pressure=0.625*p, diameter=d,
          nelems=6, timestep=1e-2, plots=False)
        vols.append(droplet_volume) # μm^3
    return vols
def diamparsweep():
    ps = [60e3, 75e3, 90e3, 105e3]
    K = 131
    n = 0.54
    dsmalls = 280, 300, 320
    dlarges = 430, 450, 470
    ds = (0, 100, 200) + dsmalls + dlarges
    with plot.PyPlot('fig', figsize=(8,5)) as fig:
        for i, p in log.enumerate('p', ps):
            vols = diamsweep(K, n, p, numpy.array(ds)*1e-6)
            fig.plot(ds, vols, '.', label=f'{p/1e3:.0f} $kPa$',
              color=pycols[i])
            y = interpolate.interp1d(ds, vols, kind='cubic')
            x = numpy.linspace(ds[0], ds[-1], 65)
            fig.plot(x, y(x), '-', label='_nolabel_', color=pycols[i])
        fig.legend()
        lims = fig.axis([0, 480, 0, 200])
        ones = numpy.ones(2)
        delta = 20
        for diam in [dsmalls[1], dlarges[1]]:
             fig.plot(diam*ones, lims[2:], '-', color='gray')
             fig.plot((diam+delta)*ones, lims[2:], ':', color='gray')
             fig.plot((diam-delta)*ones, lims[2:], ':', color='gray')
             fig.text(diam, lims[3], f'⌀ {diam:.0f}±{delta:.0f} $μm$',
                  ha='right', va='top', rotation=90)
        fig.axis(lims)
        fig.xlabel('Needle diameter [$μm$]')
        fig.ylabel(f'Droplet volume [$10^6\;μm^3$]')

        fig2 = fig.twinx()
        v = numpy.asarray(fig.gca().get_yticks())
        h = 220 # μm
        a = 3*v/h*1e2 # 10^6 μm2 --> 10^4 μm2
        fig2.set_ylabel(f'Droplet area [$10^4\;μm^2$]')
        fig2.set_yticks(v)
        fig2.set_yticklabels(lmap(lambda ai: '%.0f'%ai, a))

def powerlaw(K=131, N=0.54):
    γ = numpy.linspace(0, 10, 33)
    μ = K * numpy.abs(γ)**(N - 1)
    with plot.PyPlot('fig') as fig:
        for maxvisc in [100, 300, 1000]:
            a = (maxvisc/K)**(1/(N-1))
            γm = numpy.sqrt(a**2 + γ**2)
            μm = K * γm**(N - 1)
            fig.semilogy(γ, μm, label=f'modified $μ \leq {maxvisc:.0f}$')
        fig.semilogy(γ, μ, '+', label='original')
        fig.legend()

if __name__ == '__main__':
	  cli.choose(diamparsweep, presparsweep, thawparsweep, powerlaw, nonnewtonian, newtonian)

# vim:foldmethod=indent:foldnestmax=1
